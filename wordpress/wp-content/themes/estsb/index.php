<?php get_header(); ?>
<?php echo do_shortcode('[URIS id=82]'); ?>
<?php echo do_shortcode('[recent_post_slider media_size="full" design="design-1"] '); ?> 
	<div class="qodef-container">
		<?php do_action('qode_startit_after_container_open'); ?>
		<div class="qodef-container-inner clearfix">
			<?php qode_startit_get_blog(qode_startit_get_default_blog_list()); ?>
		</div>
		<?php do_action('qode_startit_before_container_close'); ?>
	</div>
<?php get_footer(); ?>