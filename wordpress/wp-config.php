<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'project-est' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[}>1]BfU<3:l(-yk@]Di0;8T!,wgJ8l}wLRC|Qxdi2&5aD.=z-t#B;Wa)d~fHeub' );
define( 'SECURE_AUTH_KEY',  '~0p bt,Yxayqk.,V5]5S4zZ*E4Fgt@PB|`PS#>>U:=/md4%MrD,m%2m=lx#lo8,a' );
define( 'LOGGED_IN_KEY',    '=!-`kgq^tZF$/x8Ks5r2]T3aC#>Z[pU0z63k-9q+;zs*)fG; BIx0.~FpF%%Lis!' );
define( 'NONCE_KEY',        'ckehYZh~ roed:e:Q`oq}<mlPII 0SY}4$jeTw#w=DtyJlFL!P~+~DSwWSpgEkpD' );
define( 'AUTH_SALT',        'W%9x2e&V`Z/F3NHuC/aWyDO5-7|97HV2T<k#8$P:9+cycf+v4kp~t]_v:H!uc%Lu' );
define( 'SECURE_AUTH_SALT', '<}(xaP(JCoH$Awi]S:!:B7LA-=VN1qc$Zh4q +M:!5&{`V=[^4nZJSk;2j;O;t ]' );
define( 'LOGGED_IN_SALT',   'Yw/NZ7x5IZd>rb]^bKI&n:>=8+g%X{264U):Zf88shtsGJXu1pef111:$}b+QWha' );
define( 'NONCE_SALT',       '8#Gvqx;q!nAXYsxaLjX5Buxw!`xokc75W5EqxhkAATUoyGLiH`Vb3RbD9FXiFq`&' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
